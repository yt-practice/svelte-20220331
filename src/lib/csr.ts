/* eslint-disable @typescript-eslint/no-explicit-any */
import { onDestroy } from 'svelte'

export const csr = (App: { new (p: any): any }, fn: () => void) => {
	if ('undefined' !== typeof document && !(window as any).__hoge) {
		;(window as any).__hoge = 1
		const target = document.querySelector('body > div')
		if (target) {
			target.innerHTML = ''
			const app = new App({ target })
			onDestroy(() => app.$destroy())
		}
	} else {
		fn()
	}
}
