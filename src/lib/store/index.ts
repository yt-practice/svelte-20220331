export type Item = Readonly<{
	id: number
	name: string
	secret: string
}>

const items: Item[] = [
	{
		id: 1,
		name: 'hoge',
		secret: 'secret1',
	},
	{
		id: 2,
		name: 'fuga',
		secret: 'secret2',
	},
	{
		id: 3,
		name: 'piyo',
		secret: 'secret3',
	},
]

export const list = () => {
	return items
}

export const find = (id: Item['id']) => {
	return items.find(i => i.id === id)
}

export const add = (name: Item['name']) => {
	const id = items.length + 1
	const item: Item = { id, name, secret: `secret${id}` }
	items.push(item)
	return item
}
