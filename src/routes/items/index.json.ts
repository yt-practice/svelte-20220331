/* eslint-disable @typescript-eslint/no-unused-vars */

import { list, add } from '$lib/store'
import type { RequestHandler } from '@sveltejs/kit'

type Emp = Record<never, never>
export type GetRes = {
	items: {
		id: number
		name: string
	}[]
}
export const get: RequestHandler<Emp, GetRes> = async () => {
	return { body: { items: list().map(({ secret: _, ...i }) => i) } }
}

export const post: RequestHandler<Emp, Emp> = async ({ request }) => {
	const data = await request.formData()
	const name = data.get('name')
	if (typeof name === 'string' && name) {
		const item = add(name)
		return {
			status: 303,
			headers: {
				location: `/items/${item.id}`,
			},
		}
	}
	return {
		status: 403,
	}
}
